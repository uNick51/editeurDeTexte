
//====================================== Fonction ExecCommand simple ======================================

$('#Bold').click(function () {                                         // Bold
    document.execCommand('bold',false,'true');
});
$('#Italic').click(function () {                                       // Italic
    document.execCommand('italic',false,'true');
});
$('#Underline').click(function () {                                    // Underline
    document.execCommand('underline',false,'true');
});
$('#barre').click(function () {                                        // strikeThrough (texte barré)
    document.execCommand('strikeThrough',false,'true');
});
$('#Align_Left').click(function () {                                   // Aligne le texte à gauche
    document.execCommand('justifyLeft',false,'true');
});
$('#Align_Right').click(function () {                                  // Aligne le texte à droite
    document.execCommand('justifyRight',false,'true');
});
$('#Align_Center').click(function () {                                 // Aligne le texte au centre
    document.execCommand('justifyCenter',false,'true');
});
$('#Puce').click(function () {                                         // Liste à puce
    document.execCommand('insertUnorderedList',false,'true');
});
$('#PuceOr').click(function () {                                       // Liste à puce numéroté
    document.execCommand('insertOrderedList',false,'true');
});
$('#BR').click(function () {                                           // insert une ligne horizontale
    document.execCommand('insertHorizontalRule',false,'true');
});
$('#Undo').click(function () {                                         // annule la dernière opération
    document.execCommand('undo',false,'true');
});
$('#Redo').click(function () {                                         // annule l'annulement
    document.execCommand('redo',false,'true');
});


//====================================== Fonction ExecCommand moins simple ======================================

$('#TextColor').click(function () {                                     // change la couleur du texte et du bouton
    var couleur = $('#TextColor').val();
    document.execCommand('foreColor',false,couleur);
    $('#BtnTextColor').css('color', $('#TextColor').val());
});
$('#TextColor').change(function () {                                    // change la couleur du texte et du bouton
    var couleur = $('#TextColor').val();
    document.execCommand('foreColor',false,couleur);
    $('#BtnTextColor').css('color', $('#TextColor').val());
});
$('#TextSur').click(function () {                                       // surligne le texte et change la couleur du bouton
    document.execCommand('hiliteColor',false,'transparent');
    $('#BtnTextSur').css('color', 'white');
});
$('#TextSur').change(function () {                                      // surligne le texte et change la couleur du bouton
    var couleurSur = $('#TextSur').val();
    document.execCommand('hiliteColor',false,couleurSur);
    $('#BtnTextSur').css('color', $('#TextSur').val());
});
$('#LinkIns').click(function () {                                       // creer un lien sur un texte en demandant le lien avec un prompt
    var Lien = prompt("Entrer l'URL du lien");
    if (Lien == null || Lien == "") {
        document.execCommand('unlink',false,'true');
    }else  {
        document.execCommand('createLink',false,Lien);
    }
});
$('#InsPhoto').change(function() {                                     // Permet d'insérer une photo
    var input = document.getElementById("InsPhoto");
    var fReader = new FileReader();
    fReader.readAsDataURL(input.files[0]);
    fReader.onloadend = function(event){
        document.execCommand("insertImage", false, event.target.result);

    }
});
$('#Smiley').click(function () {                                        // Affiche la liste de smiley
    if ($('#ListeSmiley').css('display') == 'none'){
        $('#ListeSmiley').css('display', 'block');
    }else{
        $('#ListeSmiley').css('display', 'none');
    }
});
$('#ListeSmiley > img').click(function () {                             // Au clic sur un smiley, ça l'ajoute dans le contenu
    document.execCommand('insertImage',false,this.src);
    $('#ListeSmiley').css('display', 'none');
});
$('#Formats').change(function () {                                      // Change la balise en titre ou paragraphe
    var Format = $('#Formats').val();
    switch (Format) {
        case "Titre 1":
            document.execCommand('formatBlock',false,'<h1>');
            break;
        case "Titre 2":
            document.execCommand('formatBlock',false,'<h2>');
            break;
        case "Titre 3":
            document.execCommand('formatBlock',false,'<h3>');
            break;
        case "Titre 4":
            document.execCommand('formatBlock',false,'<h4>');
            break;
        case "Titre 5":
            document.execCommand('formatBlock',false,'<h5>');
            break;
        case "Titre 6":
            document.execCommand('formatBlock',false,'<h6>');
            break;
        case "Paragraphe":
            document.execCommand('formatBlock',false,'<p>');
            break;
    }
});
$('#Police').click(function () {                                        // Change la police dans le contenu et sur le select
    var Police = $('#Police').val();
    document.execCommand('fontName',false,Police);
    switch (Police) {
        case "Arctik":
            $('#Police').attr('class', 'Arctik');
            var LaPolice = "Arctik";
            break;
        case "Arial":
            $('#Police').attr('class', 'Arial');
            var LaPolice = "Arial";
            break;
        case "Champagne":
            $('#Police').attr('class', 'Champagne');
            var LaPolice = "Champagne";
            break;
        case "Courier New":
            $('#Police').attr('class', 'Courier');
            var LaPolice = "Courier";
            break;
        case "Diskontented":
            $('#Police').attr('class', 'Diskontented');
            var LaPolice = "Diskontented";
            break;
        case "Keep Singing":
            $('#Police').attr('class', 'Keep');
            var LaPolice = "Keep";
            break;
    }
    $('.content').addClass( LaPolice );
});
$('#Police').change(function () {                                       // Change la police dans le contenu et sur le select
    var Police = $('#Police').val();
    document.execCommand('fontName',false,Police);
    switch (Police) {
        case "Arctik":
            $('#Police').attr('class', 'Arctik');
            var LaPolice = "Arctik";
            break;
        case "Arial":
            $('#Police').attr('class', 'Arial');
            var LaPolice = "Arial";
            break;
        case "Champagne":
            $('#Police').attr('class', 'Champagne');
            var LaPolice = "Champagne";
            break;
        case "Courier New":
            $('#Police').attr('class', 'Courier');
            var LaPolice = "Courier";
            break;
        case "Diskontented":
            $('#Police').attr('class', 'Diskontented');
            var LaPolice = "Diskontented";
            break;
        case "Keep Singing":
            $('#Police').attr('class', 'Keep');
            var LaPolice = "Keep";
            break;
    }
    $('.content').addClass( LaPolice );
});
$('#Taille').click(function () {                                        // Change la taille de la police en pixel
var Taille = $('#Taille').val();
    document.execCommand("fontSize", false, "5");
    var fontElements = document.getElementsByTagName("font");
    for (var i = 0, len = fontElements.length; i < len; ++i) {
        if (fontElements[i].size == "5") {
            fontElements[i].removeAttribute("size");
            fontElements[i].style.fontSize = Taille + "px";
        }
}
});
