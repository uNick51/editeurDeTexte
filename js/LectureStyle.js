
//====================================== Initialisation ======================================

for (var i = 0; i < localStorage.length; i++) {
    console.log('LocalStorage : ' + localStorage.key(i));
    Decode(localStorage.getItem(localStorage.key(i)));

}

//====================================== Fonction ======================================

function Decode(Value) {                    //Lit la value du localstorage et la converti pour appliquer les style sur l'option qui ajouter
    var Para = Value.substring(0,1),
        Police = Value.substring(1,2),
        Size = Value.substring(2,4),
        Bold = Value.substring(4,5),
        Italic = Value.substring(5,6),
        Ligne = Value.substring(6,7),
        TextColor = Value.substring(7,14),
        Stabilo = Value.substring(14);
    console.log('Para : ' + Para);
    console.log('Police : ' + Police);
    console.log('Size : ' + Size);
    console.log('Bold : ' + Bold);
    console.log('Italic : ' + Italic);
    console.log('Ligne : ' + Ligne);
    console.log('TextColor : ' + TextColor);
    console.log('Stabilo : ' + Stabilo);

    var SPolice;                      // converti la police
    switch (Police) {
        case "0":
            SPolice = "Arctik";
            break;
        case "1":
            SPolice = "Arial";
            break;
        case "2":
            SPolice = "Champagne";
            break;
        case "3":
            SPolice = "Courier";
            break;
        case "4":
            SPolice = "Diskontented";
            break;
        case "5":
            SPolice = "Keep Singing";
            break;
    }

    var Sbold;                          // Converti le bold
    if (Bold == 0){
        Sbold = "normal";
    } else {
        Sbold = "bold";
    }

    var SItalic;                       // Converti l' Italic
    if (Italic == 0){
        SItalic = "normal";
    } else {
        SItalic = "italic";
    }

    var SLigne;                        // Converti en underline et/ou line-through
    if (Ligne == 0){
        SLigne = "initial";
    } else if (Ligne == 1) {
        SLigne = "underline";
    } else if (Ligne == 2) {
        SLigne = "line-through";
    } else {
        SLigne = "underline line-through";
    }

    var SPara;                       // Converti en futur tag
    switch (Para) {
        case "0":
            SPara = "p";
            break;
        case "1":
            SPara = "h1";
            break;
        case "2":
            SPara = "h2";
            break;
        case "3":
            SPara = "h3";
            break;
        case "4":
            SPara = "h4";
            break;
        case "5":
            SPara = "h5";
            break;
        case "6":
            SPara = "h6";
            break;
    }
                                    // Ligne qui ajoute l'option avec les styles et la class qui deviendra un tag

    $('#Style').append('<option class="'+ SPara + '" style="font-family: '+SPolice+';font-size: '+Size+'px;font-weight: '+Sbold+';font-style: '+SItalic+';text-decoration: '+ SLigne + ';color: '+TextColor+';background-color: '+Stabilo+'">' + localStorage.key(i) + '</option>');
}

$('#Style').change(function () {          // fonction qui change le texte selectionné par le style personnalisé

    if ($('#Style').val() == "Style par défaut") {      // selection du style par défaut avec application du style de base et mise en paragraphe
        LeStyle = 'font-family: Arial;font-size: 16px;font-weight: normal;font-style: normal;text-decoration: initial;color: #ffffff;background-color: transparent';

        document.execCommand("fontSize", false, "5");
        var fontElements = document.getElementsByTagName("font");
        for (var i = 0, len = fontElements.length; i < len; ++i) {
            if (fontElements[i].size == "5") {
                fontElements[i].removeAttribute("size");
                fontElements[i].style = LeStyle;
            }
        }
        document.execCommand('formatBlock',false,'<p>');
    } else {                                               //selection du style personnalisé
        var LaClass = $('#Style :selected').attr('class'),             // récupération de la class du style (p,h1,h2,...)
            LeStyle = $('#Style :selected').attr('style');             // récupération des styles du style Perso

        document.execCommand("fontSize", false, "5");
        var fontElements = document.getElementsByTagName("font");
        for (var i = 0, len = fontElements.length; i < len; ++i) {
            if (fontElements[i].size == "5") {
                fontElements[i].removeAttribute("size");
                fontElements[i].style = LeStyle;                               // application des styles
            }
        }

        switch (LaClass) {                                                     // changement du Tag en ce servant de la class
            case "h1":
                document.execCommand('formatBlock',false,'<h1>');
                break;
            case "h2":
                document.execCommand('formatBlock',false,'<h2>');
                break;
            case "h3":
                document.execCommand('formatBlock',false,'<h3>');
                break;
            case "h4":
                document.execCommand('formatBlock',false,'<h4>');
                break;
            case "h5":
                document.execCommand('formatBlock',false,'<h5>');
                break;
            case "h6":
                document.execCommand('formatBlock',false,'<h6>');
                break;
            case "p":
                document.execCommand('formatBlock',false,'<p>');
                break;
        }
    }

});