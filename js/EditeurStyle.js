
//====================================== Variables ======================================

var Para = 0,
    Police = "0",
    Size = 16,
    Bold = 0,
    Italic = 0,
    Ligne = 0,
    TextColor = "#ffffff",
    Stabilo = "transparent";

//====================================== Initialisation ======================================

$( "#EditeurStyle" ).toggle();
$( ".Gris" ).toggle();

//====================================== Fonction ======================================

$('#CreerStyle').click(function () {            // Affichage de la Modal de création de style
        $( "#EditeurStyle" ).toggle();
        $( ".Gris" ).toggle();
});

$('#Style').click(function () {                // Affichage de la Modal de création de style
    var Style = $('#Style').val();
    if (Style == "Créer un style") {
        $( "#EditeurStyle" ).toggle();
        $( ".Gris" ).toggle();
    }
});

$('#Style').change(function () {              // Affichage de la Modal de création de style
    var Style = $('#Style').val();
    if (Style == "Créer un style") {
        $( "#EditeurStyle" ).toggle();
        $( ".Gris" ).toggle();
    }
});

$( "#ModClose" ).click(function () {          // Croix pour fermer la Modal de création de style
    $( "#EditeurStyle" ).toggle();
    $( ".Gris" ).toggle();
});

$('#ModFormats').change(function () {          // Changer le tag du contenu en gardant la class et le style
    var texte = $('#LeContenu').text();
    var id = "LeContenu";
    var lesclass = document.getElementById('LeContenu').className;
    var Format = $('#ModFormats').val();
    var LeStyle = document.getElementById('LeContenu').style.backgroundColor;
    switch (Format) {
        case "Titre 1":
            $('#LeContenu').replaceWith('<h1 id="' + id + '" class="' + lesclass + '" style="background-color: ' + LeStyle + '">' + texte + '</h1>');
            Para = 1;
            break;
        case "Titre 2":
            $('#LeContenu').replaceWith('<h2 id="' + id + '" class="' + lesclass + '" style="background-color: ' + LeStyle + '">' + texte + '</h2>');
            Para = 2;
            break;
        case "Titre 3":
            $('#LeContenu').replaceWith('<h3 id="' + id + '" class="' + lesclass + '" style="background-color: ' + LeStyle + '">' + texte + '</h3>');
            Para = 3;
            break;
        case "Titre 4":
            $('#LeContenu').replaceWith('<h4 id="' + id + '" class="' + lesclass + '" style="background-color: ' + LeStyle + '">' + texte + '</h4>');
            Para = 4;
            break;
        case "Titre 5":
            $('#LeContenu').replaceWith('<h5 id="' + id + '" class="' + lesclass + '" style="background-color: ' + LeStyle + '">' + texte + '</h5>');
            Para = 5;
            break;
        case "Titre 6":
            $('#LeContenu').replaceWith('<h6 id="' + id + '" class="' + lesclass + '" style="background-color: ' + LeStyle + '">' + texte + '</h6>');
            Para = 6;
            break;
        case "Paragraphe":
            $('#LeContenu').replaceWith('<p id="' + id + '" class="' + lesclass + '" style="background-color: ' + LeStyle + '">' + texte + '</p>');
            Para = 0;
            break;
    }
});
$('#ModPolice').click(function () {                // Changer la police du contenu et afficher la police dans le select avec la bonne police
    var PoliceVal = $('#ModPolice').val();
    switch (PoliceVal) {
        case "Arctik":
            $('#ModPolice').attr('class', 'Arctik');
            var LaPolice = "Arctik";
            Police = "0";
            break;
        case "Arial":
            $('#ModPolice').attr('class', 'Arial');
            var LaPolice = "Arial";
            Police = "1";
            break;
        case "Champagne":
            $('#ModPolice').attr('class', 'Champagne');
            var LaPolice = "Champagne";
            Police = "2";
            break;
        case "Courier New":
            $('#ModPolice').attr('class', 'Courier');
            var LaPolice = "Courier";
            Police = "3";
            break;
        case "Diskontented":
            $('#ModPolice').attr('class', 'Diskontented');
            var LaPolice = "Diskontented";
            Police = "4";
            break;
        case "Keep Singing":
            $('#ModPolice').attr('class', 'Keep');
            var LaPolice = "Keep Singing";
            Police = "5";
            break;
    }
    $('.Modcontent').css("font-family", LaPolice );
});
$('#ModPolice').change(function () {           // Changer la police du contenu et afficher la police dans le select avec la bonne police
    var PoliceVal = $('#ModPolice').val();
    switch (PoliceVal) {
        case "Arctik":
            $('#ModPolice').attr('class', 'Arctik');
            var LaPolice = "Arctik";
            Police = "0";
            break;
        case "Arial":
            $('#ModPolice').attr('class', 'Arial');
            var LaPolice = "Arial";
            Police = "1";
            break;
        case "Champagne":
            $('#ModPolice').attr('class', 'Champagne');
            var LaPolice = "Champagne";
            Police = "2";
            break;
        case "Courier New":
            $('#ModPolice').attr('class', 'Courier');
            var LaPolice = "Courier";
            Police = "3";
            break;
        case "Diskontented":
            $('#ModPolice').attr('class', 'Diskontented');
            var LaPolice = "Diskontented";
            Police = "4";
            break;
        case "Keep Singing":
            $('#ModPolice').attr('class', 'Keep');
            var LaPolice = "Keep Singing";
            Police = "5";
            break;
    }
    $('.Modcontent').css("font-family", LaPolice );
});
$('#ModTextColor').click(function () {             // Modifie la couleur du texte du contenu
    var couleur = $('#ModTextColor').val();
    $('.Modcontent').css('color', couleur);
    $('#ModBtnTextColor').css('color', couleur);
    TextColor = couleur;
});
$('#ModTextColor').change(function () {            // Modifie la couleur du texte du contenu
    var couleur = $('#ModTextColor').val();
    $('.Modcontent').css('color', couleur);
    $('#ModBtnTextColor').css('color', couleur);
    TextColor = couleur;
});
$('#ModTextSur').click(function () {                // Enleve le surlignement
    $('#LeContenu').removeClass('Hilite');
    $('#LeContenu').css('background-color', 'transparent');
    $('#ModBtnTextSur').css('color', 'white');
    Stabilo = 'transparent';
});
$('#ModTextSur').change(function () {               // Ajoute le surlignement en fonction de la couleur selectionné et change le bouton de couleur
    $('#LeContenu').addClass('Hilite');
    var couleurSur = $('#ModTextSur').val();
    $('#LeContenu').css('background-color', couleurSur);
    $('#ModBtnTextSur').css('color', couleurSur);
    Stabilo = couleurSur;
});
$('#ModBold').click(function () {                  // Met ou enleve en bold au click
    if (Bold == 0){
        $('.Modcontent').css('font-weight', 'bold');
        Bold = 1;
    } else {
        $('.Modcontent').css('font-weight', 'normal');
        Bold = 0;
    }
});
$('#ModItalic').click(function () {               // Met ou enleve en italic au click
    if (Italic == 0){
        $('.Modcontent').css('font-style', 'italic');
        Italic = 1;
    } else {
        $('.Modcontent').css('font-style', 'normal');
        Italic = 0;
    }
});
$('#ModUnderline').click(function () {                  // souligne le texte ou enleve le soulignement du texte
    if (Ligne == 0){
        $('.Modcontent').css('text-decoration', 'underline');
        Ligne = 1;
    } else if (Ligne == 1) {
        $('.Modcontent').css('text-decoration', 'initial');
        Ligne = 0;
    } else if (Ligne == 2) {
        $('.Modcontent').css('text-decoration', 'underline line-through');
        Ligne = 3;
    } else {
        $('.Modcontent').css('text-decoration', 'line-through');
        Ligne = 2;
    }
});
$('#Modbarre').click(function () {                 // barre le texte ou enleve la barre du texte
    if (Ligne == 0){
        $('.Modcontent').css('text-decoration', 'line-through');
        Ligne = 2;
    } else if (Ligne == 1) {
        $('.Modcontent').css('text-decoration', 'underline line-through');
        Ligne = 3;
    } else if (Ligne == 2) {
        $('.Modcontent').css('text-decoration', 'initial');
        Ligne = 0;
    } else {
        $('.Modcontent').css('text-decoration', 'underline');
        Ligne = 1;
    }
});
$('#ModTaille').click(function () {                  // Change la taille du texte
    var Taille = $('#ModTaille').val();
    $('.Modcontent').css({fontSize: Taille + "px"});
    Size = Taille;
});

document.getElementById("LeContenu").addEventListener("input", function() {      // change le nom du style en fonction de ce qui est ecris dans le contenu
    $('#NomStyle').text($('#LeContenu').text());
}, false);

$('#BtnEnregistrer').click(function () {     // enregistre en localstorage le nom du style en key et les valeur en value puis ferme la modal
    localStorage.setItem($('#LeContenu').text(), Para + Police + Size + Bold + Italic + Ligne + TextColor + Stabilo);
    $( "#EditeurStyle" ).toggle();
    $( ".Gris" ).toggle();
});
$('#ModUndo').click(function () {                                         // annule la dernière opération
    document.execCommand('undo',false,'true');
});
$('#ModRedo').click(function () {                                         // annule l'annulement
    document.execCommand('redo',false,'true');
});